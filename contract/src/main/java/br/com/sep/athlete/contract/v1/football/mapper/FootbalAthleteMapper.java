package br.com.sep.athlete.contract.v1.football.mapper;

import br.com.sep.athlete.contract.v1.football.model.FootballAthlete;
import br.com.sep.athlete.impl.football.enumeration.FootballPlayerPositionEnum;
import br.com.sep.athlete.impl.football.model.FootballAthleteModel;
import com.google.common.collect.Lists;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

public class FootbalAthleteMapper {
    public static List<FootballAthlete> mapListFromImpl(List<FootballAthleteModel> footballAthleteModelList) {
        if (ObjectUtils.isEmpty(footballAthleteModelList)) {
            return Lists.newArrayList();
        }
        return footballAthleteModelList.stream()
                .map(FootbalAthleteMapper::mapFromImpl)
                .collect(Collectors.toList());
    }

    public static FootballAthlete mapFromImpl(FootballAthleteModel footballAthleteModel) {
        if (ObjectUtils.isEmpty(footballAthleteModel)) {
            return new FootballAthlete();
        }
        return FootballAthlete.builder()
                .name(footballAthleteModel.getName())
                .position(footballAthleteModel.getPosition())
                .age(footballAthleteModel.getAge())
                .build();
    }

    public static FootballAthleteModel mapToImpl(FootballAthlete footballAthlete) {
        return FootballAthleteModel.builder()
                .name(footballAthlete.getName())
                .position(FootballPlayerPositionEnum.fromName(footballAthlete.getPosition()).getName())
                .age(footballAthlete.getAge())
                .build();
    }

}
