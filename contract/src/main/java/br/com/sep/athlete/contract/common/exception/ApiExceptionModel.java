package br.com.sep.athlete.contract.common.exception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Default exception class")
public class ApiExceptionModel {
    @ApiModelProperty
    private Integer code;

    @ApiModelProperty
    private String message;
}
