package br.com.sep.athlete.contract.common.exception;

import br.com.sep.athlete.impl.common.exception.ApiException;
import br.com.sep.athlete.impl.common.exception.DataNotFoundException;
import br.com.sep.athlete.impl.common.exception.DatabaseException;
import br.com.sep.athlete.impl.common.exception.InvalidInputException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(InvalidInputException.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ApiExceptionModel handleInvalidInputException(ApiException exception) {
        return ApiExceptionModel.builder()
                .code(exception.code())
                .message(exception.message())
                .build();
    }

    @ExceptionHandler(DataNotFoundException.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public ApiExceptionModel handleClientNotFoundException(ApiException exception) {
        return ApiExceptionModel.builder()
                .code(exception.code())
                .message(exception.message())
                .build();
    }

    @ExceptionHandler(DatabaseException.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiExceptionModel handleDatabaseException(ApiException exception) {
        return ApiExceptionModel.builder()
                .code(exception.code())
                .message(exception.message())
                .build();
    }
}
