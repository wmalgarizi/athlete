package br.com.sep.athlete.contract.v1.football.facade;

import br.com.sep.athlete.contract.v1.football.mapper.FootbalAthleteMapper;
import br.com.sep.athlete.contract.v1.football.model.FootballAthlete;
import br.com.sep.athlete.impl.football.service.FootballAthleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FootballAthleteFacade {
    @Autowired
    private FootballAthleteService footballAthleteService;

    public FootballAthlete findAthleteById(Long id) {
        return FootbalAthleteMapper.mapFromImpl(footballAthleteService.findAthleteById(id));
    }

    public List<FootballAthlete> findAll() {
        return FootbalAthleteMapper.mapListFromImpl(footballAthleteService.findAll());
    }

    public void insertNew(FootballAthlete footballAthlete) {
        footballAthleteService.insertNew(FootbalAthleteMapper.mapToImpl(footballAthlete));
    }
}
