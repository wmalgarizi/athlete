package br.com.sep.athlete.contract.v1.football;

import br.com.sep.athlete.contract.common.exception.ApiExceptionModel;
import br.com.sep.athlete.contract.v1.football.facade.FootballAthleteFacade;
import br.com.sep.athlete.contract.v1.football.model.FootballAthlete;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/v1/football")
public class FootballRestController {
    @Autowired
    private FootballAthleteFacade footballAthleteFacade;

    @CrossOrigin(allowedHeaders = {"*"})
    @GetMapping(path = "/id/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Find football athlete by id", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = FootballAthlete[].class),
            @ApiResponse(code = 404, message = "None football athlete was found", response = ApiExceptionModel.class),
            @ApiResponse(code = 500,
                    message = "Database communication failure has occurred, if the problem persists, contact support.",
                    response = ApiExceptionModel.class)
    })
    public FootballAthlete findAthleteById(@PathVariable(value = "id") Long id) {
        return footballAthleteFacade.findAthleteById(id);
    }

    @CrossOrigin(allowedHeaders = {"*"})
    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Find all football athletes", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = FootballAthlete[].class),
            @ApiResponse(code = 404, message = "None football athlete was found", response = ApiExceptionModel.class),
            @ApiResponse(code = 500,
                    message = "Database communication failure has occurred, if the problem persists, contact support.",
                    response = ApiExceptionModel.class)
    })
    public List<FootballAthlete> findAll() {
        return footballAthleteFacade.findAll();
    }

    @CrossOrigin(allowedHeaders = {"*"})
    @PostMapping(path = "/new",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Create new Footbal athlete",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = FootballAthlete[].class),
            @ApiResponse(code = 500,
                    message = "Database communication failure has occurred, if the problem persists, contact support.",
                    response = ApiExceptionModel.class)
    })
    public void insertNew(@Valid @RequestBody FootballAthlete footballAthlete) {
        footballAthleteFacade.insertNew(footballAthlete);
    }
}
