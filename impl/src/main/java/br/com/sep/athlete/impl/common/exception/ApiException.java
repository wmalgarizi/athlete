package br.com.sep.athlete.impl.common.exception;

public abstract class ApiException extends RuntimeException {
    public abstract Integer code();
    public abstract String message();
}
