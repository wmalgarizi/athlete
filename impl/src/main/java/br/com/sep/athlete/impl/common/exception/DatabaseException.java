package br.com.sep.athlete.impl.common.exception;

public class DatabaseException extends ApiException {
    private static final String MESSAGE = "Failed to access database, please contact support.";

    @Override
    public Integer code() {
        return null;
    }

    @Override
    public String message() {
        return MESSAGE;
    }
}
