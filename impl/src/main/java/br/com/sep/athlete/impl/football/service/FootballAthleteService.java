package br.com.sep.athlete.impl.football.service;

import br.com.sep.athlete.impl.football.model.FootballAthleteModel;
import br.com.sep.athlete.impl.football.repository.FootballAthleteRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FootballAthleteService {
    @Autowired
    private FootballAthleteRepositoryImpl footballAthleteRepositoryImpl;

    public FootballAthleteModel findAthleteById(Long id) {
        return footballAthleteRepositoryImpl.findAthleteById(id);
    }

    public List<FootballAthleteModel> findAll() {
        return footballAthleteRepositoryImpl.findAllFootbalAthletes();
    }

    public void insertNew(FootballAthleteModel footballAthleteModel) {
        footballAthleteRepositoryImpl.insertNewFootballAthlete(footballAthleteModel);
    }

}
