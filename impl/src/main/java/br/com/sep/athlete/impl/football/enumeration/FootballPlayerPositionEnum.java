package br.com.sep.athlete.impl.football.enumeration;

import br.com.sep.athlete.impl.common.exception.InvalidInputException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FootballPlayerPositionEnum {
    QB(1, "QUARTER_BACK"),
    RB(2, "RUNNING_BACK"),
    R(3, "RECEIVER"),
    TE(4, "TIGHT_END");

    private Integer code;
    private String name;

    public static FootballPlayerPositionEnum fromName(String name) {
        for (FootballPlayerPositionEnum position : FootballPlayerPositionEnum.values()) {
            if (position.getName().equals(name)) {
                return position;
            }
        }
        throw new InvalidInputException("The position is not valid");
    }
}
