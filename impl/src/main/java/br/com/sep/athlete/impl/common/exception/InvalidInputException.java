package br.com.sep.athlete.impl.common.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class InvalidInputException extends ApiException {
    private String message;

    @Override
    public Integer code() {
        return null;
    }

    @Override
    public String message() {
        return this.message;
    }
}
