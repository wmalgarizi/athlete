package br.com.sep.athlete.impl.football.repository;

import br.com.sep.athlete.impl.football.model.FootballAthleteModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface FootballAthleteRepository extends JpaRepository<FootballAthleteModel, Long> {
}
