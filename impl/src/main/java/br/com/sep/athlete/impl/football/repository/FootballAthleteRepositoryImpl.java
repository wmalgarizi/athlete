package br.com.sep.athlete.impl.football.repository;

import br.com.sep.athlete.impl.common.exception.DataNotFoundException;
import br.com.sep.athlete.impl.common.exception.DatabaseException;
import br.com.sep.athlete.impl.football.model.FootballAthleteModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class FootballAthleteRepositoryImpl {
    @Autowired
    private FootballAthleteRepository footballAthleteRepository;

    public FootballAthleteModel findAthleteById(Long id) {
        try {
            return footballAthleteRepository.findById(id)
                    .filter(footballAthletes -> !ObjectUtils.isEmpty(footballAthletes))
                    .orElseThrow(() -> new DataNotFoundException("None athlete was found"));
        } catch (DataAccessException error) {
            log.error(" error method - findAllFootbalAthletes ", error.getMessage());
            throw new DatabaseException();
        }
    }

    public List<FootballAthleteModel> findAllFootbalAthletes() {
        try {
            return Optional.ofNullable(footballAthleteRepository.findAll())
                    .filter(footballAthletes -> !ObjectUtils.isEmpty(footballAthletes))
                    .orElseThrow(() -> new DataNotFoundException("None athlete was found"));
        } catch (DataAccessException error) {
            log.error(" error method - findAllFootbalAthletes ", error.getMessage());
            throw new DatabaseException();
        }
    }

    public void insertNewFootballAthlete(FootballAthleteModel footballAthleteModel) {
        try {
            footballAthleteRepository.save(footballAthleteModel);
        } catch (DataAccessException error) {
            log.error(" error method - insertNewFootballAthlete ", error.getMessage());
            throw new DatabaseException();
        }
    }
}
